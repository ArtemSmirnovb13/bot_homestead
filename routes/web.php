<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::match(['get', 'post'], '/botman', 'BotManController@handle');
Route::get('/botman/tinker', 'BotManController@tinker');
Route::get('debug', 'DebugController@index');
Auth::routes(['register' => true]);
//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home_test', 'HomeController@saveBD')->name('home.inject');
Route::post('/del', 'HomeController@deleteR')->name('home.del');
Route::post('/del_all', 'HomeController@DeleteAll')->name('home.DeleteAll');
Route::get('/horizon')->name('horizon');

Route::delete('{id}', 'HomeController@destroy')->name('home.destroy');

Horizon::auth(function ($request) {
    return $request->user();
});