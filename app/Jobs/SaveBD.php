<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\WordsStat;
use Mockery\Exception;

class SaveBD implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws FileNotFoundException
     */
    public function handle()
    {
        try {
            $contents = Storage::get('file.log');
            $count = $this->WordsCount($contents);

            foreach ($count as $key => $item) {


                $word = DB::table('words_stats')->where('word', $key)->first();

                if ($word != null) {

                    DB::table('words_stats')->where('id', $word->id)->update([
                        'count' => $word->count + $item
                    ]);

                } else {
                    $db = new WordsStat();
                    $db->word = $key;
                    $db->count = $item;
                    $db->save();
                }

            }
            $this->SaveToDBTwoWords();
            Storage::delete('file.log');

            return redirect()->route('home');

        } catch (Exception $e) {
            $e->getMessage();

        }
    }

    private function WordsCount($contents)
    {
        $words = explode(',', $contents);
        $out = [];
        foreach ($words as $word) {
            isset($out[$word]) ? $out[$word]++ : $out[$word] = 1;
        }
        arsort($out);
        return $out;
    }

    private function SaveToDBTwoWords(){
        try {
            $contents = Storage::get('file2.log');

            $count = $this->WordsCount($contents);

            foreach ($count as $key => $item) {

                $word = DB::table('words_stats')->where('word', $key)->first();

                if ($word != null) {

                    DB::table('words_stats')->where('id', $word->id)->update([
                        'count' => $word->count + $item
                    ]);

                } else {
                    $db = new WordsStat();
                    $db->word = $key;
                    $db->count = $item;
                    $db->save();

                }

            }
            Storage::delete('file2.log');


        } catch (FileNotFoundException $e) {
            echo 'error FileNotFound';
        }
    }
}
