<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class SaveTwoWordsToText implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $text;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->text = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $text = mb_strtolower($this->text);
        $tranform_text = explode(' ', $text);
        if (count($tranform_text) >= 2) {
            $finall_text2 = array_chunk($tranform_text, 2, TRUE);
            for ($i = 0; $i < count($finall_text2); $i++) {
                $two_words [] = implode(' ', $finall_text2[$i]);
                $result = implode(',', $two_words);
            }
            Storage::append('file2.log', $result,',');
        }
    }
}
