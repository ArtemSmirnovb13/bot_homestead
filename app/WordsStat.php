<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WordsStat extends Model
{
    protected $table = 'words_stats';

    /**
     * @var array
     */
    protected $fillable
        = [
            'id',
            'word',
            'count',
            'created_at',
            'updated_at'
        ];
}
