<?php

namespace App\Conversations;

use App\Jobs\SaveTwoWordsToText;
use App\Jobs\SaveWordsToText;
use App\messengerUser as database;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\BotMan\Messages\Conversations\Conversation;
use App\Dictionary;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Incoming\Answer as BotManAnswer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;
use BotMan\BotMan\Messages\Outgoing\Question as BotManQuestion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class mainConversation extends Conversation
{
    public $response = [];

    public function run()
    {
        //$this->words();
        $this->words2();
    }

    public function isVideo(){
        $arr =$this->bot->getMessage()->getPayload();
        if (isset($arr['video'])) {
            $result = true;
        }else
        {
            $result = false;
        }
        return $result;
    }

    private function words2(){
        if ($this->isVideo() == false) {
            $text = $this->bot->getMessage()->getText();

            $text = mb_strtolower($text);
            $arrText2 = explode(' ', $text);
            $collection = collect($arrText2);
            $ARR = DB::table('dictionaries')->get();
            $ARRCOPY = $ARR;
            foreach ($ARR->all() as $item) {
                $item->key = json_decode($item->key);
            }
            for ($w = 0; $w < $collection->count(); $w++) {
                foreach ($ARR as $item) {
                    $COUNTER = count($item->key);
                    for ($i = 0; $i < $COUNTER; $i++) {

                        if ($item->key[$i] == $collection[$w]) {
                            $item->key [$i] = 'true';
                        }
                    }

                }
            }
            foreach ($ARR as $item) {
                $COUNTER = count($item->key);
                for ($i = 0; $i < $COUNTER; $i++) {

                    if ($item->key[$i] != 'true') {
                        $item->key [$i] = 'false';
                    }
                }

            }

            for ($i = 0; $i < $ARR->count(); $i++) {
                $k = 0;
                $l = 0;
                for ($j = 0; $j < count($ARR[$i]->key); $j++) {

                    if ($ARR[$i]->key[$j] == 'false') {
                        $count_false[$i] = ['count_false' => $k += 1, 'count_true' => $l += 0, 'id' => $ARR[$i]->id];

                    } elseif ($ARR[$i]->key[$j] == 'true') {
                        $count_false[$i] = ['count_false' => $k += 0, 'count_true' => $l += 1, 'id' => $ARR[$i]->id];
                    }


                }
            }
            $collect_count_false = collect($count_false);
            $sorted = $collect_count_false->sortBy('count_false');
            $sorted_min = $sorted->min('count_false');
            $filtered = $sorted->where('count_false', $sorted_min)->all();
            $filtered_false_not_null = collect($filtered)->where('count_false', '==', 0);
            if ($filtered_false_not_null->isEmpty() != true) {
                $win_key = $filtered_false_not_null->random();
                if (rand(0, 1) == 0) {

                    //dd($ARRCOPY->where('id','=',$win_key['id'])->first()->answer);
                    $answer = json_decode($ARRCOPY->where('id', '=', $win_key['id'])->first()->answer);
                    $say = $answer[array_rand($answer, 1)];
                    $this->sendMessage($this->bot->getMessage()->getRecipient(), $say, $this->bot->getMessage()->getPayload()['message_id']);
                }
            }
            //$this->exit();
            //$this->SaveWordsToText($this->bot->getMessage()->getText());
            $getMess = $this->bot->getMessage()->getText();
            if ($getMess != '/add') {
                SaveWordsToText::dispatch($getMess)->onQueue('hight');
                // $this->SaveTwoWordsToText($this->bot->getMessage()->getText());
                SaveTwoWordsToText::dispatch($getMess)->onQueue('hight');
            }
        }else {
            info('errorISVIDEO');
        }
    }

    private function SaveWordsToText($text){
        $tranform_text = explode(' ', $text);
        $finall_text = implode(',',$tranform_text);

        Storage::append('file.log', $finall_text,',');
    }


    /**
     * @param $text
     */
    private function SaveTwoWordsToText($text){
        $text = mb_strtolower($text);
        $tranform_text = explode(' ', $text);
        if (count($tranform_text) >= 2) {
            $finall_text2 = array_chunk($tranform_text, 2, TRUE);
            for ($i = 0; $i < count($finall_text2); $i++) {
                $two_words [] = implode(' ', $finall_text2[$i]);
                $result = implode(',', $two_words);
            }
            Storage::append('file2.log', $result,',');
        }

    }


    private function words()
    {

        $arr2 = DB::table('dictionaries')->pluck('answer', 'key');
        $text = $this->bot->getMessage()->getText();
        $result = [];
        foreach ($arr2 as $k => $v) {

            if (mb_stripos($text, $k) === false) {

            } else {
                $result[$k] = +true;
            }
        }
        if ($result != null) {
            $win_key  = array_rand($result,1);
            if (rand(0,1) == 0) {
                $answer = json_decode($arr2[$win_key]);
                $say = $answer[array_rand($answer, 1)];
               // $this->say($say);
            //    info('debug', array($this->bot->getMessage()->getPayload()['message_id']));
             //   info('debug', array($this->bot->getMessage()->getPayload()));
             //   info('debug', array($this->bot->getMessage()->getRecipient()));
               // $this->sendMessage($this->bot->getUser()->getId(),$say, $this->bot->getMessage()->getPayload()['message_id']);
                $this->sendMessage($this->bot->getMessage()->getRecipient(),$say, $this->bot->getMessage()->getPayload()['message_id']);
            }
        }

        $this->exit();
    }

    /**
     * @param $chat_id
     * @param $say
     * @param $reply_to_message_id
     */
    private function sendMessage($chat_id,$say,$reply_to_message_id)
    {

        \Telegram\Bot\Laravel\Facades\Telegram::sendMessage([
            'chat_id' => $chat_id,
            'text' => $say,
            'reply_to_message_id' => $reply_to_message_id
        ]);
    }

    private function askWeather()
    {
        $question = BotManQuestion::create("Тебе нравится погода на улице?");

        $question->addButtons([
            Button::create('Да')->value(1),
            Button::create('Нет')->value(2)
        ]);

        $this->ask($question, function (BotManAnswer $answer) {
            // здесь можно указать какие либо условия, но нам это не нужно сейчас

            array_push($this->response, $answer);

            $this->exit();
        });
    }

    private function exit()
    {

        $user = database::where('id_chat', '=', $this->bot->getUser()->getId())->first();
        if ($user === null) {
            $db = new database();
            $db->id_chat = $this->bot->getUser()->getId();
            $db->name = $this->bot->getUser()->getFirstName();
            $db->response = $this->bot->getMessage()->getText();
            $db->js = json_encode(['']);
            $db->save();
        }
        else {
            DB::table('messenger_users')->where('id_chat','=', $this->bot->getUser()->getId())->update([
                'id_chat' =>$this->bot->getUser()->getId(),
                'name' => $this->bot->getUser()->getFirstName(),
                'js' => json_encode(['']),
                'response' => $this->bot->getMessage()->getText(),
            ]);
        }




        return true;
    }
}
