<?php

namespace App\Conversations;

use BotMan\BotMan\Messages\Conversations\Conversation;
use App\messengerUser as database;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Messages\Attachments\Image;
use App\Dictionary;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Incoming\Answer as BotManAnswer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;
use BotMan\BotMan\Messages\Incoming\IncomingMessage;
use BotMan\BotMan\Messages\Outgoing\Question as BotManQuestion;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Support\Facades\DB;

class addToDict extends Conversation
{
    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->Dict();
    }

    private function Dict()
    {


        $question = BotManQuestion::create("Хотите добавить связку в словарь ?");

        $question->addButtons([
            Button::create('Да')->value(1),
            Button::create('Нет')->value(2)
        ]);
        $this->ask($question, function (Answer $answer) {

            if ($answer->getValue() == 1) {
                $this->addDict();
            } else {
                $this->bot->reply('good bye');
            }
        });

    }

    private function addDict()
    {
        $this->ask('Пожалуйста введите что там хотели $a==$b', function (Answer $answer) {
            $text = $this->bot->getMessage()->getText();
            if ($this->new_bd($text) == true) {
                $this->say('good');
            } else {
                $this->say('bad');
            }

        });
    }

    private function new_bd($text)
    {
        try {
            $arr = explode('==', $text);
            $key = mb_strtolower($arr[0]);
            $mess = $arr[1];
            $arr_keys = explode(' ', $key);
            $ARR = DB::table('dictionaries')->where('key', json_encode($arr_keys,JSON_UNESCAPED_UNICODE))->first();
            if ($ARR != null) {
                $var1 = json_decode($ARR->answer);
                $var2 = $mess;
                array_push($var1, $var2);
                DB::table('dictionaries')->where('key', json_encode($arr_keys,JSON_UNESCAPED_UNICODE))->update([
                    'answer' => json_encode($var1,JSON_UNESCAPED_UNICODE)
                ]);
            } else {
                $db = new Dictionary();
                $db->key = json_encode($arr_keys,JSON_UNESCAPED_UNICODE);
                $db->answer = json_encode([$mess],JSON_UNESCAPED_UNICODE);
                $db->save();
            }

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function BD($text)
    {
        try {

            $arr = explode('==', $text);
            $key = $arr[0];
            $mess = $arr[1];
            $object = DB::table('dictionaries')->where('key', '=', $key)->first();
            if ($object != null) {
                $var1 = json_decode($object->answer);
                $var2 = $mess;
                array_push($var1, $var2);
                DB::table('dictionaries')->where('key', '=', $key)->update([
                    'answer' => json_encode($var1)
                ]);

            } else {
                $db = new Dictionary();
                $db->key = $key;
                $db->answer = json_encode([$mess]);
                $db->save();
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
