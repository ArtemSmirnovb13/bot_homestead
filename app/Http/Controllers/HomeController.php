<?php

namespace App\Http\Controllers;

use App\Charts\UserChart;
use App\Jobs\ClearAllDataBase;
use App\Jobs\ClearRecycle;
use App\Jobs\SaveBD;
use App\User;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\WordsStat;
use App\WordsStat as DelWord;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $borderColors = [
            "rgba(255, 99, 132, 1.0)",
            "rgba(22,160,133, 1.0)",
            "rgba(255, 205, 86, 1.0)",
            "rgba(51,105,232, 1.0)",
            "rgba(244,67,54, 1.0)",
            "rgba(34,198,246, 1.0)",
            "rgba(153, 102, 255, 1.0)",
            "rgba(255, 159, 64, 1.0)",
            "rgba(233,30,99, 1.0)",
            "rgba(205,220,57, 1.0)"
        ];
        $fillColors = [
            "rgba(255, 99, 132, 0.2)",
            "rgba(22,160,133, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(51,105,232, 0.2)",
            "rgba(244,67,54, 0.2)",
            "rgba(34,198,246, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(233,30,99, 0.2)",
            "rgba(205,220,57, 0.2)"

        ];
        $word = DB::table('words_stats')->orderByDesc('count')->paginate(10);

        $wordChart = DB::table('words_stats')->orderByDesc('count')->get();
        $usersChart = new UserChart;
        if ($word->items() != null) {
            $usersChart->minimalist(true);
            if (isset($wordChart->get(0)->word) && isset($wordChart->get(1)->word) && isset($wordChart->get(2)->word) && isset($wordChart->get(3)->word) && isset($wordChart->get(4)->word) && isset($wordChart->get(5)->word)){
            $usersChart->labels([$wordChart->get(0)->word,
                $wordChart->get(1)->word,
                $wordChart->get(2)->word,
                $wordChart->get(3)->word,
                $wordChart->get(4)->word,
                $wordChart->get(5)->word
            ]);
            }else{
                $usersChart->labels(['Jan', 'Feb', 'Mar']);
            }
            if (isset( $wordChart->get(0)->count) && isset( $wordChart->get(1)->count) && isset( $wordChart->get(2)->count) && isset( $wordChart->get(3)->count) && isset( $wordChart->get(4)->count) && isset( $wordChart->get(5)->count)) {
                $usersChart->dataset('Users by trimester', 'doughnut', [
                    $wordChart->get(0)->count,
                    $wordChart->get(1)->count,
                    $wordChart->get(2)->count,
                    $wordChart->get(3)->count,
                    $wordChart->get(4)->count,
                    $wordChart->get(5)->count
                ])->color($borderColors)
                    ->backgroundcolor($fillColors);
            }else {
                $usersChart->dataset('Users by trimester', 'line', [10, 25, 13]);
            }
        }else{

            $usersChart->labels(['Jan', 'Feb', 'Mar']);
            $usersChart->dataset('Users by trimester', 'line', [10, 25, 13]);
        }



        return view('home', [
            'words' => $word,
            'usersChart' => $usersChart
        ]);
    }
    public function deleteR()
    {
//        DB::table('words_stats')->where('word', '=', '%%%_AUDIO_%%%')->delete();
//        DB::table('words_stats')->where('word', '=', ' ')->delete();
//        DB::table('words_stats')->where('word', '=', '%%%_IMAGE_%%%')->delete();
//        DB::table('words_stats')->where('word', '=', '%%%_VIDEO_%%%')->delete();
        ClearRecycle::dispatch();
        return redirect()->route('home');
    }

    public function DeleteAll(){
       // DB::table('words_stats')->delete();
        ClearAllDataBase::dispatch()->onQueue('low')->delay(now()->addSeconds(10));
        return redirect()->route('home');
    }

    private function SaveToDBTwoWords(){
        try {
            $contents = Storage::get('file2.log');

            $count = $this->WordsCount($contents);

            foreach ($count as $key => $item) {


                $word = DB::table('words_stats')->where('word', $key)->first();

                if ($word != null) {

                    DB::table('words_stats')->where('id', $word->id)->update([
                        'count' => $word->count + $item
                    ]);
                    // sleep(1);
                } else {
                    $db = new WordsStat();
                    $db->word = $key;
                    $db->count = $item;
                    $db->save();
                    //   sleep(1);
                }

            }
            Storage::delete('file2.log');


        } catch (FileNotFoundException $e) {
            echo 'error FileNotFound';
        }
    }

    public function saveBD()
    {
        SaveBD::dispatch();
        return redirect()->route('home');
//        try {
//            $contents = Storage::get('file.log');
//            $count = $this->WordsCount($contents);
//
//            foreach ($count as $key => $item) {
//
//
//                $word = DB::table('words_stats')->where('word', $key)->first();
//
//                if ($word != null) {
//
//                    DB::table('words_stats')->where('id', $word->id)->update([
//                        'count' => $word->count + $item
//                    ]);
//                    // sleep(1);
//                } else {
//                    $db = new WordsStat();
//                    $db->word = $key;
//                    $db->count = $item;
//                    $db->save();
//                    //   sleep(1);
//                }
//
//            }
//            $this->SaveToDBTwoWords();
//            Storage::delete('file.log');
//
//            return redirect()->route('home');
//
//        } catch (FileNotFoundException $e) {
//            echo 'error FileNotFound';
//            sleep(2);
//            return redirect()->route('home');
//        }


    }

    private function WordsCount($contents)
    {
        $words = explode(',', $contents);
        $out = [];
        foreach ($words as $word) {
            isset($out[$word]) ? $out[$word]++ : $out[$word] = 1;
        }
        arsort($out);
        return $out;
    }


    public function destroy($id)
    {
        WordsStat::destroy($id);
        return redirect()->route('home');
    }
}
