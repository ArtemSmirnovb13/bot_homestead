@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Words</th>
                                <th scope="col">Count</th>
                                <th scope="col"> </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($words as $word)
                                <tr>
                                    <td>{{$word->id}}</td>
                                    <td>{{$word->word}}</td>
                                    <td>{{$word->count}}</td>
                                    <td>
                                        <form action="{{ route('home.destroy', ['id' => $word->id]) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button class="btn btn-sm btn-danger" type="submit">Удалить</button>
                                        </form>
                                    </td>
                                </tr>

                            @empty
                                <tr>
                                    <td colspan="6" class="text-center"><h2>Данные отстутсвуют</h2></td>
                                </tr>
                            @endforelse
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="5">
                                    {{ $words->links() }}
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                        <form class="form-horizontal" action="{{route('home.inject')}}" method="post">
                            {{csrf_field()}}
                            {{ method_field('POST') }}
                            {{--form include--}}


                            <div class="row myrow">

                                <div class="col-sm pb-2" style="display: grid">
                                    <input class="btn btn-primary" name="action" type="submit" value="test">
                                </div>


                            </div>

                    </form>
                            <form class="form-horizontal" action="{{route('home.del')}}" method="post">
                                {{csrf_field()}}
                                {{ method_field('POST') }}
                                <div class="row myrow">
                                    <div class="col-sm pb-2" style="display: grid">
                                        <input class="btn btn-primary" name="action" type="submit" value="Удалить мусор">
                                    </div>
                                </div>
                            </form>
                            <form class="form-horizontal" action="{{route('home.DeleteAll')}}" method="post">
                                {{csrf_field()}}
                                {{ method_field('POST') }}
                                <div class="row myrow">
                                    <div class="col-sm pb-2" style="display: grid">
                                        <input class="btn btn-primary" name="action" type="submit" value="Удалить всё">
                                    </div>
                                </div>
                            </form>
                            <a class="btn btn-primary" href="{{route('horizon')}}" role="button">dachboard</a>

                </div>
                <div style="width: 50%">
                    {!! $usersChart->container() !!}
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
